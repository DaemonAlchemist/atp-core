/**
 * Created by Andy on 8/25/2016.
 */

import * as handlebars from "./handlebars.js";

handlebars.registerHelpers({
    css: (path, media, options) => {
        if(typeof options == 'undefined') {
            options = media;
            media = "screen";
        }
        getLoader().addCss(path, media);

        return "";
    }
});

class Loader {
    constructor(){
        this.templateLoaders = {};
        this.css = {};
        //this.templates["module/login/views/login-form"] = "Test loader template";
    }

    addCss(path, media) {
        let key = path + ":" + media;
        if(typeof this.css[key] == 'undefined') {
            $("head").append(Handlebars.compile('<link rel="stylesheet" media="{{media}}", href="{{path}}" />')({
                path: path,
                media: media
            }));
            this.css[key] = true;
        }
    }

    template(path) {
        let $this = this;
        console.log("Loading template: " + path);
        //TODO:  Allow theme overriding of templates
        if(typeof $this.templateLoaders[path] == 'undefined') {
            $this.templateLoaders[path] = new Promise((resolve, reject) => {
                $.ajax({
                    url: path + ".hbt.html",
                    success: (data, textStatus, jqXHR) => {
                        console.log("Template " + path + " loaded");
                        //Register this template as a partial and compile it
                        Handlebars.registerPartial(path, data);
                        let compiledTemplate = Handlebars.compile(data);

                        //Find all partials in this template
                        let partials = data.match(/{{>([\sa-zA-Z0-9\/\-\_]*)}}/g);
                        if(partials) {
                            console.log("Template " + path + " has partials");
                            //This template isn't ready until all partials it needs are also loaded
                            Promise.all(_.uniq(partials)
                                //Strip the partial delimiters
                                .map(partial => partial.replace("{{>", "").replace("}}", "").trim().split(" ")[0])
                                //Load the partials
                                .map(partial => $this.template(partial))
                            )
                            .then(() => {resolve(compiledTemplate);})
                            .catch(reject);
                        } else {
                            console.log("Template " + path + " has NO partials");
                            resolve(compiledTemplate);
                        }
                    },
                    error: reject
                });
            });
        }
        return $this.templateLoaders[path];
    }
}

let loader = new Loader();
function getLoader() {
    return loader;
}

export default loader;
