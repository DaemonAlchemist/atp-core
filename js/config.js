/**
 * Created by Andy on 8/28/2016.
 */

class Config {
    constructor() {
        this.config = {};
    }

    add(config) {
        $.extend(this.config, config);
    }

    get() {
        return this.config;
    }
}

var config = new Config();
export default config;