/**
 * Created by Andy on 8/26/2016.
 */

import homepageController from "./controller/homepage-controller.js";
import navbarController from "./controller/navbar-controller.js";

export default () => {
    homepageController();
    navbarController();
}