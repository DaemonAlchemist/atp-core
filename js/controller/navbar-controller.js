/**
 * Created by Andy on 8/26/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import dashboard from "../../../core/js/dashboard.js";
import config from "../../../core/js/config.js";

export default function navbarController() {
    router.addRoute(/^.*$/, () => {
        console.log("Navbar controller");
        if(rest.isLoggedIn()) {
            load.template('module/core/views/navbar').then((navbar) => {
                //dashboard.initMenus();
                $("#navbar").html(navbar({
                    brand: config.get().theme.siteName,
                    menusLeft: dashboard.getMenus('left'),
                    menusRight: dashboard.getMenus('right')
                }));
            });
        } else {
            $("#navbar").html("");
        }
    });
}
