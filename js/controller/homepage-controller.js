/**
 * Created by Andy on 8/26/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import dashboard from "../../../core/js/dashboard.js";

export default function homepageController() {
    router.addRoute(/^(home|)$/, () => {
        console.log("Homepage controller");
        load.template('module/core/views/homepage-template').then(homepage => {
            $("#page-content").html(homepage());

            var container = $("#widgets");
            var widgets = dashboard.getWidgets();
            for(var name in widgets) {
                container.append('<div id="' + name + '"></div>');
                widgets[name]($("div#" + name));
            }
        });
    });
}
