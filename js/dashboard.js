/**
 * Created by Andy on 8/26/2016.
 */

class Dashboard
{
    constructor() {
        this.menus = {};
        this.widgets = {};
    }

    getWidgets() {
        return this.widgets;
    }

    addWidget(name, func) {
        this.widgets[name] = func;
    }

    getMenus(location) {
        var menus = {};
        for(name in this.menus) {
            if(this.menus[name].location == location) {
                menus[name] = this.menus[name];
            }
        }
        return menus;
    }

    addMenu(key, data, container) {
        if (typeof container == 'undefined') {
            container = this.menus;
        }

        var keyParts = key.split(":");
        var first = keyParts.shift();

        if (typeof container[first] == 'undefined') {
            container[first] = {
                title: "&lt;Undefined&gt;",
                icon: 'fa fa-question-mark',
                location: 'left',
                url: "",
                permissions: [],
                children: {}
            };
        }

        if (keyParts.length == 0) {
            $.extend(container[first], data);
        } else {
            this.addMenu(keyParts.join(":"), data, container[first].children);
        }
    }
}

var dashboard = new Dashboard();

export default dashboard;