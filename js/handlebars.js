/**
 * Created by Andy on 8/25/2016.
 */

export function registerHelpers(helpers) {
    for(var helper in helpers) {
        Handlebars.registerHelper(helper, helpers[helper]);
    }
}
