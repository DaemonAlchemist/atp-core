/**
 * Created by Andy on 8/20/2016.
 */

import * as handlebars from "./handlebars.js";

var pageTitleSegments = [];
$(document).on('atp.page.new', () => {
    clearTitle();
    addTitle('ATP');
});

function clearTitle() {
    pageTitleSegments = [];
    $("head title").html("");
}

function addTitle(title) {
    pageTitleSegments.push(title);
    $("head title").html(pageTitleSegments.reverse().join(" - "));
}

handlebars.registerHelpers({
    pageTitle: (title) => {
        addTitle(title);
        return "";
    }
});

class Router {
    constructor() {
        this.routes = [];
    }

    addRoute(route, controller) {
        this.routes.push({route, controller});
    }

    run(route) {
        console.log("Router running " + route);
        let hash = "#" + route;
        if(location.hash != hash) {
            location.hash = hash;
        }
        return this.routes
            .filter(info => route.match(info.route))
            .map(info => info.controller.apply(null, route.match(info.route).slice(1)));
    }
}

var router = new Router();
export default router;
