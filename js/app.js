/**
 * Created by Andy on 8/24/2016.
 */

import router from "./router.js";
import rest from "../../../module/rest/js/rest.js";
import config from "./config.js";

class App {
    constructor() {
    }

    start() {
        console.log("App starting");

        rest.connect(config.get().rest.url);

        let $this = this;
        window.onhashchange = function() {
            $this.run();
        }

        $(document).trigger('atp.app.start');

        console.log("App started");

        this.run();
    }

    run() {
        console.log("App running");
        $(document).trigger('atp.page.new');
        $(document).trigger('atp.app.run');
        try {
            console.log("Running router");
            router.run(location.hash.substr(1));
        } catch (e) {
            console.log("Exception thrown: " + e);
            //Do nothing
        }
        console.log("App complete");
        $(document).trigger('atp.app.complete');
    }

    pageContainer() {
        return $("#page-content");
    }
}

var app = new App();
export default app;
